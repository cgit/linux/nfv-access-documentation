#!/bin/bash
set -e

# Change PATH environment variable
export PATH=/rdtools/docbook/jdk1.6.0_45/bin:/rdtools/docbook/fop-2.1:$PATH

# Get script arguments
docs_path=$1
if [ "$docs_path" = "" ]
then
    echo " usage: build.sh <build_doc_path>"
    exit
fi

# Build documentation
echo make dist BOOK_DIST_DIR=${docs_path} BOOK_DIST_ECLIPSE=yes
make dist BOOK_DIST_DIR=${docs_path} BOOK_DIST_ECLIPSE=yes

echo "Build documentation was successful. Documentation can be found here ${docs_path}"
